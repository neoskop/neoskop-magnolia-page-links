# README

Allgemeine Beschreibung vom Modul **Page Links**.

# Abhängigkeiten

- [Magnolia CMS][1] >= 5.3.7

# Installation

Als Repository muss [JitPack](https://jitpack.io/) in der `pom.xml` des Magnolia-Projektes hinzugefügt werden:

```xml
<repositories>
	<repository>
		<id>jitpack.io</id>
		<url>https://jitpack.io</url>
	</repository>
</repositories>
```

Das Modul muss dann in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-page-links</artifactId>
	<version>2.1.1</version>
</dependency>
```

# Veröffentlichung neuer Versionen

Um eine neue Version des Moduls auf [unserem Nexus][2] als Snapshot oder Release zu hinterlegen, muss in der Maven-Konfigurations-Datei - unter Unix-Systemen in der Datei `~/.m2/settings.xml` - folgender Server-Eintrag hinzugefügt werden:

```xml
<settings>
  <servers>
    <server>
      <id>neoskop.nexus.deployment</id>
      <username>deployment</username>
      <password>password</password>
    </server>
  </servers>
</settings>
```

Ein Snapshot kann mit dem Maven-Goal `deploy` auf den Nexus gespeichert werden.

Eine Release-Version des Moduls kann mit dem [Maven Release Plug-In][3] auf dem Nexus veröffentlicht werden. Dazu müssen die folgenden beiden Maven-Befehler abgesetzt werden:

```bash
mvn release:prepare
mvn release:perform
```

Mehr Informationen zur Benutzung

[1]: https://www.magnolia-cms.com
[2]: http://maven.neoskop.io
[3]: http://maven.apache.org/maven-release/maven-release-plugin/
