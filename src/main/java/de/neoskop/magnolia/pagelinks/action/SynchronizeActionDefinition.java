package de.neoskop.magnolia.pagelinks.action;

import info.magnolia.ui.api.action.CommandActionDefinition;

/**
 * @author Arne Diekmann
 * @since 17.09.15
 */
public class SynchronizeActionDefinition extends CommandActionDefinition {

  public SynchronizeActionDefinition() {
    setImplementationClass(SynchronizeAction.class);
  }
}
