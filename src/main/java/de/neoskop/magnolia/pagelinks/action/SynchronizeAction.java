package de.neoskop.magnolia.pagelinks.action;

import info.magnolia.commands.CommandsManager;
import info.magnolia.event.EventBus;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.framework.action.AbstractCommandAction;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemUtil;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Arne Diekmann
 * @since 17.09.15
 */
public class SynchronizeAction extends AbstractCommandAction<SynchronizeActionDefinition> {

  private final EventBus eventBus;
  private final JcrItemAdapter item;

  @Inject
  protected SynchronizeAction(
      final SynchronizeActionDefinition definition,
      final JcrItemAdapter item,
      final CommandsManager commandsManager,
      UiContext uiContext,
      SimpleTranslator i18n,
      @Named(SubAppEventBus.NAME) EventBus eventBus) {
    super(definition, item, commandsManager, uiContext, i18n);
    this.eventBus = eventBus;
    this.item = item;
  }

  @Override
  protected String getSuccessMessage() {
    return getDefinition().getSuccessMessage();
  }

  @Override
  protected String getFailureMessage() {
    return getDefinition().getFailureMessage();
  }

  @Override
  protected void onPostExecute() throws Exception {
    super.onPostExecute();
    Object itemId = JcrItemUtil.getItemId(item.getJcrItem());
    eventBus.fireEvent(new ContentChangedEvent(itemId, true));
  }
}
