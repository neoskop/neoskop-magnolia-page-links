package de.neoskop.magnolia.pagelinks.action;

import com.vaadin.data.Item;
import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.dialog.action.SaveDialogAction;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.form.EditorValidator;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arne Diekmann
 * @since 01.09.15
 */
public class CreateLinkSaveDialogAction<T extends CreateLinkSaveDialogActionDefinition>
    extends SaveDialogAction {

  private static final Logger LOG = LoggerFactory.getLogger(CreateLinkSaveDialogAction.class);
  private PageLinkService service;

  public CreateLinkSaveDialogAction(
      T definition, Item item, EditorValidator validator, EditorCallback callback) {
    super(definition, item, validator, callback);
  }

  @Inject
  public CreateLinkSaveDialogAction(
      T definition,
      Item item,
      EditorValidator validator,
      EditorCallback callback,
      PageLinkService service) {
    super(definition, item, validator, callback);
    this.service = service;
  }

  @Override
  public void execute() throws ActionExecutionException {
    validator.showValidation(true);
    if (validator.isValid()) {
      JcrNodeAdapter itemChanged = (JcrNodeAdapter) item;
      Node source;

      try {
        source = itemChanged.applyChanges();
        setNodeName(source, itemChanged);
        final String targetPath = source.getProperty("parentNode").getString();
        service.createLink(source, targetPath);
      } catch (final RepositoryException e) {
        throw new ActionExecutionException(e);
      }

      callback.onSuccess(getDefinition().getName());
    } else {
      LOG.info("Validation error(s) occurred. No save performed.");
    }
  }
}
