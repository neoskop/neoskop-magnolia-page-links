package de.neoskop.magnolia.pagelinks.action;

import info.magnolia.ui.api.action.CommandActionDefinition;

/**
 * @author Arne Diekmann
 * @since 15.05.17
 */
public class SyncLinksActionDefinition extends CommandActionDefinition {

  public SyncLinksActionDefinition() {
    setImplementationClass(SyncLinksAction.class);
  }
}
