package de.neoskop.magnolia.pagelinks.action;

import info.magnolia.ui.dialog.action.SaveDialogActionDefinition;

/**
 * @author Arne Diekmann
 * @since 01.09.15
 */
public class CreateLinkSaveDialogActionDefinition extends SaveDialogActionDefinition {

  public CreateLinkSaveDialogActionDefinition() {
    setImplementationClass(CreateLinkSaveDialogAction.class);
  }
}
