package de.neoskop.magnolia.pagelinks.action;

import info.magnolia.ui.api.action.CommandActionDefinition;

/**
 * @author Arne Diekmann
 * @since 31.08.15
 */
public class BreakLinkActionDefinition extends CommandActionDefinition {

  public BreakLinkActionDefinition() {
    setImplementationClass(BreakLinkAction.class);
  }
}
