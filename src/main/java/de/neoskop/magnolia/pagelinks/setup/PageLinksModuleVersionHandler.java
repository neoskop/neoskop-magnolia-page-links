package de.neoskop.magnolia.pagelinks.setup;

import static info.magnolia.jcr.nodebuilder.Ops.setProperty;

import info.magnolia.jcr.nodebuilder.task.ErrorHandling;
import info.magnolia.jcr.nodebuilder.task.NodeBuilderTask;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleModuleResource;
import info.magnolia.module.delta.CheckOrCreatePropertyTask;
import info.magnolia.module.delta.CreateNodePathTask;
import info.magnolia.module.delta.DeltaBuilder;
import info.magnolia.module.delta.ModuleBootstrapTask;
import info.magnolia.module.delta.NodeExistsDelegateTask;
import info.magnolia.module.delta.RemoveNodeTask;
import info.magnolia.module.delta.RemovePropertyTask;
import info.magnolia.module.delta.Task;
import info.magnolia.repository.RepositoryConstants;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is optional and lets you manager the versions of your module, by registering "deltas"
 * to maintain the module's configuration, or other type of content. If you don't need this, simply
 * remove the reference to this class in the module descriptor xml.
 */
public class PageLinksModuleVersionHandler extends DefaultModuleVersionHandler {

  public PageLinksModuleVersionHandler() {
    register(DeltaBuilder.update("1.0.1", "").addTask(new ModuleBootstrapTask()));
    register(
        DeltaBuilder.update("1.0.2", "")
            .addTask(
                new RemovePropertyTask(
                    "Remove untranslated label",
                    "/modules/pages/apps/pages/subApps/browser/actions/createLink",
                    "label")));
    register(DeltaBuilder.update("1.0.3", "").addTask(new ModuleBootstrapTask()));
    register(
        DeltaBuilder.update("1.0.7", "")
            .addTask(
                new CheckOrCreatePropertyTask(
                    "",
                    "/modules/observation/config/listenerConfigurations/propagateChangesToLinks",
                    "delay",
                    "500")));
    register(DeltaBuilder.update("1.0.8", "").addTask(new ModuleBootstrapTask()));
    register(
        DeltaBuilder.update("2.0.0", "")
            .addTask(
                new NodeExistsDelegateTask(
                    "",
                    "/modules/observation/config/listenerConfigurations/propagateChangesToLinks",
                    new RemoveNodeTask(
                        "",
                        "/modules/observation/config/listenerConfigurations/propagateChangesToLinks")))
            .addTask(
                new BootstrapSingleModuleResource(
                    "config.modules.pages.apps.pages.subApps.browser.actions.syncLinks.xml"))
            .addTask(
                new BootstrapSingleModuleResource(
                    "config.modules.pages.apps.pages.subApps.browser.actionbar.sections.pageActions.groups.pageLinkActions.xml"))
            .addTask(new BootstrapSingleModuleResource("config.modules.page-links.commands.xml")));
    register(
        DeltaBuilder.update("2.0.1", "")
            .addTask(
                new BootstrapSingleModuleResource(
                    "config.modules.pages.apps.pages.subApps.browser.actions.syncLinks.xml")));
  }

  @Override
  protected List<Task> getExtraInstallTasks(InstallContext installContext) {
    List<Task> tasks = new ArrayList<>();
    tasks.add(
        new NodeExistsDelegateTask(
            "",
            "/modules/pages/apps/pages/subApps/browser/workbench/contentViews/tree/columns/page",
            null,
            new CreateNodePathTask(
                "",
                "/modules/pages/apps/pages/subApps/browser/workbench/contentViews/tree/columns/page",
                NodeTypes.Content.NAME)));
    tasks.add(
        new NodeBuilderTask(
            "Set formatterClass for pages de.neoskop.magnolia.pagelinks.LinkNameColumnFormatter",
            "",
            ErrorHandling.logging,
            RepositoryConstants.CONFIG,
            "/modules/pages/apps/pages/subApps/browser/workbench/contentViews/tree/columns/page",
            setProperty(
                "formatterClass", "de.neoskop.magnolia.pagelinks.LinkNameColumnFormatter")));
    return tasks;
  }
}
