package de.neoskop.magnolia.pagelinks.service;

import com.google.common.collect.Lists;
import de.neoskop.magnolia.pagelinks.PageLinksModule;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.StreamSupport;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arne Diekmann
 * @since 31.08.15
 */
@Singleton
public class PageLinkService {

  public static final String MIXIN_NAME = "mgnl:isLink";
  public static final String DEVIATION_MIXIN_NAME = "mgnl:hasDeviated";
  public static final String LINK_ATTRIBUTE_NAME = "mgnl:linkTo";
  private static final Logger LOG = LoggerFactory.getLogger(PageLinkService.class);
  private final PageLinksModule module;

  @Inject
  public PageLinkService(PageLinksModule module) {
    this.module = module;
  }

  public boolean isLink(Node node) {
    try {
      return NodeUtil.hasMixin(node, MIXIN_NAME);
    } catch (RepositoryException e) {
      LOG.error("checking for mixin '" + MIXIN_NAME + "' failed", e);
      return false;
    }
  }

  public String getSourcePath(Node node) {
    if (!isLink(node)) {
      throw new IllegalArgumentException("Node is not a link!");
    }

    try {
      return node.getProperty(LINK_ATTRIBUTE_NAME).getNode().getPath();
    } catch (RepositoryException e) {
      LOG.error("Getting the weak Reference failed", e);
    }

    return null;
  }

  public Node createLink(Node source, String targetPath) {
    try {
      Node target = copyNode(source, targetPath);
      linkPageAndSubpages(source, target);
      target.getSession().save();
      return target;
    } catch (RepositoryException e) {
      LOG.error("adding of mixin '" + MIXIN_NAME + "' failed", e);
    }

    return null;
  }

  private void linkPageAndSubpages(Node source, Node target) throws RepositoryException {
    String sourceUUID = source.getIdentifier();

    if (StringUtils.isNotBlank(sourceUUID)) {
      if (target.canAddMixin(MIXIN_NAME)) {
        target.addMixin(MIXIN_NAME);
      } else {
        LOG.debug(
            "Couldn't add {} to {} ({} -> {})",
            MIXIN_NAME,
            target.getPath(),
            LINK_ATTRIBUTE_NAME,
            sourceUUID);
      }

      target.setProperty(LINK_ATTRIBUTE_NAME, sourceUUID, PropertyType.WEAKREFERENCE);

      if (StringUtils.isNotBlank(module.getCanonicalTagField())) {
        target.setProperty(module.getCanonicalTagField(), sourceUUID);
      }
    }

    NodeIterator nodeIt = source.getNodes();

    while (nodeIt.hasNext()) {
      Node newSource = nodeIt.nextNode();

      if (newSource.isNodeType(NodeTypes.Page.NAME)) {
        Node newTarget = target.getNode(newSource.getName());
        linkPageAndSubpages(newSource, newTarget);
      }
    }
  }

  public void breakLink(Node node) {
    try {
      if (node.isNodeType(NodeTypes.Page.NAME)) {
        if (StringUtils.isNotBlank(module.getCanonicalTagField())) {
          node.getProperty(module.getCanonicalTagField()).remove();
        }

        node.getProperty(LINK_ATTRIBUTE_NAME).remove();
        node.removeMixin(MIXIN_NAME);
      } else if (node.isNodeType(NodeTypes.Component.NAME)) {
        node.addMixin(DEVIATION_MIXIN_NAME);
      }

      node.getSession().save();
    } catch (RepositoryException e) {
      LOG.error("removing of link mixin failed", e);
    }
  }

  private Node copyNode(Node source, String parentNodePath) throws RepositoryException {
    NodeUtil.copyInSession(source, parentNodePath + "/" + source.getName());
    final Session session = source.getSession();
    final Node copiedNode = session.getNode(parentNodePath + "/" + source.getName());
    session.save();
    return copiedNode;
  }

  public void synchronizePart(Node node) throws RepositoryException {
    if (!NodeUtil.hasMixin(node, DEVIATION_MIXIN_NAME)) {
      throw new IllegalArgumentException(
          "Given node is not deviated: " + NodeUtil.getPathIfPossible(node));
    }

    if (node.isNodeType(NodeTypes.Page.NAME)) {
      synchronizePageProperties(node);
    } else if (node.isNodeType(NodeTypes.Area.NAME)) {
      synchronizeArea(node);
    }
  }

  public void synchronizePageProperties(Node pageNode) throws RepositoryException {
    if (!pageNode.isNodeType(NodeTypes.Page.NAME)) {
      throw new IllegalArgumentException(
          "Node "
              + NodeUtil.getNodePathIfPossible(pageNode)
              + " is not a page but of type "
              + pageNode.getPrimaryNodeType());
    }

    Node sourceNode = pageNode.getProperty(LINK_ATTRIBUTE_NAME).getNode();

    PropertyIterator propertyIt = pageNode.getProperties();

    while (propertyIt.hasNext()) {
      Property property = propertyIt.nextProperty();
      String propertyName = property.getName();

      if (isUnrestrictedProperty(propertyName)) {
        property.remove();
      }
    }

    propertyIt = sourceNode.getProperties();

    while (propertyIt.hasNext()) {
      Property property = propertyIt.nextProperty();
      String propName = property.getName();

      if (!pageNode.hasProperty(propName) && isUnrestrictedProperty(propName)) {
        if (property.isMultiple()) {
          pageNode.setProperty(propName, property.getValues());
        } else {
          pageNode.setProperty(propName, property.getValue());
        }
      }
    }

    if (NodeUtil.hasMixin(pageNode, DEVIATION_MIXIN_NAME)) {
      pageNode.removeMixin(DEVIATION_MIXIN_NAME);
    }

    pageNode.getSession().save();
  }

  public boolean isUnrestrictedProperty(String propertyName) {
    return !module.getSynchronizedSystemProps().contains(propertyName)
        && !module.getCanonicalTagField().equals(propertyName)
        && !LINK_ATTRIBUTE_NAME.equals(propertyName)
        && !propertyName.startsWith("jcr:");
  }

  public void deviate(Node node) throws RepositoryException {
    if (!node.isNodeType(NodeTypes.Area.NAME) && !node.isNodeType(NodeTypes.Page.NAME)) {
      throw new IllegalArgumentException(
          "Given node is not an area or page but a " + node.getPrimaryNodeType().getName());
    }

    node.addMixin(DEVIATION_MIXIN_NAME);
    node.getSession().save();
  }

  public List<Node> findLinks(Node node) {
    try {
      return findLinks(node.getIdentifier(), node.getSession());
    } catch (RepositoryException e) {
      LOG.warn("Could not access links of " + NodeUtil.getPathIfPossible(node), e);
      return Collections.emptyList();
    }
  }

  public List<Node> findLinks(String identifier, Session session) throws RepositoryException {
    final QueryManager queryManager = session.getWorkspace().getQueryManager();
    final String statement =
        "SELECT * FROM ["
            + PageLinkService.MIXIN_NAME
            + "] WHERE ["
            + PageLinkService.LINK_ATTRIBUTE_NAME
            + "] = '"
            + identifier
            + "'";
    final Query query = queryManager.createQuery(statement, "JCR-SQL2");
    final NodeIterator nodeIt = query.execute().getNodes();
    List<Node> result = new ArrayList<>();

    while (nodeIt.hasNext()) {
      result.add(nodeIt.nextNode());
    }

    return result;
  }

  public boolean isPageAClone(Node pageNode) {
    final NodeIterator nodeIt;

    try {
      nodeIt = pageNode.getNodes();

      while (nodeIt.hasNext()) {
        final Node areaNode = nodeIt.nextNode();

        if (areaNode.isNodeType(NodeTypes.Area.NAME)
            && NodeUtil.hasMixin(areaNode, PageLinkService.DEVIATION_MIXIN_NAME)) {
          return false;
        }
      }

      return !NodeUtil.hasMixin(pageNode, PageLinkService.DEVIATION_MIXIN_NAME);
    } catch (RepositoryException e) {
      LOG.error("Assessing if the node is a clone failed", e);
    }

    return false;
  }

  public boolean isInSync(Node node, String relativePath) {
    return relativePath.contains("/")
        ? isAreaInSync(node, relativePath)
        : isPagePropertiesInSync(node, relativePath);
  }

  private boolean isPagePropertiesInSync(Node link, String relativePath) {
    // If the path does not concern the topmost properties, allow changes right away
    if (relativePath.contains("/")) {
      return true;
    }

    try {
      return !NodeUtil.hasMixin(link, PageLinkService.DEVIATION_MIXIN_NAME);
    } catch (RepositoryException e) {
      LOG.error("Could not check for mixin presence of " + PageLinkService.DEVIATION_MIXIN_NAME, e);
      return false;
    }
  }

  private boolean isAreaInSync(Node l, String relativePath) {
    ArrayList<String> dirs = Lists.newArrayList(relativePath.split("/"));
    Node currentNode = l;

    for (String dir : dirs) {
      try {
        if (currentNode.isNodeType(NodeTypes.Area.NAME)
            && NodeUtil.hasMixin(currentNode, PageLinkService.DEVIATION_MIXIN_NAME)) {
          return false;
        }

        if (!currentNode.hasNode(dir)) {
          return true;
        }

        currentNode = currentNode.getNode(dir);
      } catch (RepositoryException e) {
        LOG.error("Checking whether a node deviated failed", e);
      }
    }

    return true;
  }

  public void synchronizeArea(Node areaNode) throws RepositoryException {
    if (!areaNode.isNodeType(NodeTypes.Area.NAME)) {
      throw new IllegalArgumentException(
          "Node "
              + NodeUtil.getNodePathIfPossible(areaNode)
              + " is not an area but of type "
              + areaNode.getPrimaryNodeType());
    }

    Node parent = areaNode.getParent();
    String name = areaNode.getName();
    areaNode.remove();
    areaNode.getSession().save();
    copyNode(parent.getProperty(LINK_ATTRIBUTE_NAME).getNode().getNode(name), parent.getPath());
  }

  public void synchronizeLinkedPages(Node pageNode, Session session) throws RepositoryException {
    if (!pageNode.isNodeType(NodeTypes.Page.NAME)) {
      throw new IllegalArgumentException(
          "Node "
              + NodeUtil.getNodePathIfPossible(pageNode)
              + " is not a page but of type "
              + pageNode.getPrimaryNodeType());
    }

    findLinks(pageNode.getIdentifier(), session).forEach(this::synchronizeLinkedPage);
  }

  public void synchronizeLinkedPage(Node linkedPageNode) {
    try {
      if (!NodeUtil.hasMixin(linkedPageNode, PageLinkService.MIXIN_NAME)) {
        throw new IllegalArgumentException(
            "Node " + NodeUtil.getNodePathIfPossible(linkedPageNode) + " is not a link");
      }

      StreamSupport.stream(
              NodeUtil.getNodes(linkedPageNode, NodeTypes.Area.NAME).spliterator(), false)
          .forEach(
              areaNode -> {
                try {
                  if (!NodeUtil.hasMixin(areaNode, PageLinkService.DEVIATION_MIXIN_NAME)) {
                    synchronizeArea(areaNode);
                  }
                } catch (RepositoryException e) {
                  LOG.error(
                      "Synchronizing area " + NodeUtil.getPathIfPossible(areaNode) + " failed", e);
                }
              });

      if (!NodeUtil.hasMixin(linkedPageNode, PageLinkService.DEVIATION_MIXIN_NAME)) {
        synchronizePageProperties(linkedPageNode);
      }
    } catch (RepositoryException e) {
      LOG.error(
          "Could not synchronize linked page node "
              + NodeUtil.getNodePathIfPossible(linkedPageNode),
          e);
    }
  }
}
