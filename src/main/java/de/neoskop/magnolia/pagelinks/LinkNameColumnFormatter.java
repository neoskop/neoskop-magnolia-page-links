package de.neoskop.magnolia.pagelinks;

import com.vaadin.ui.Table;
import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.ui.workbench.column.AbstractColumnFormatter;
import info.magnolia.ui.workbench.column.definition.ColumnDefinition;
import javax.inject.Inject;
import javax.jcr.Item;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arne Diekmann
 * @since 31.08.15
 */
public class LinkNameColumnFormatter extends AbstractColumnFormatter<ColumnDefinition> {

  private static final Logger LOG = LoggerFactory.getLogger(LinkNameColumnFormatter.class);
  private static final String ICON = "icon-link";

  private final PageLinkService service;

  @Inject
  public LinkNameColumnFormatter(ColumnDefinition definition, PageLinkService service) {
    super(definition);
    this.service = service;
  }

  @Override
  public String generateCell(Table source, Object itemId, Object columnId) {
    Item jcrItem = getJcrItem(source, itemId);

    if (jcrItem != null && jcrItem.isNode()) {
      Node node = (Node) jcrItem;
      StringBuilder pageName = new StringBuilder();

      try {
        if (service.isLink(node)) {
          pageName.append(node.getName());
          pageName.append("<span title=\"");
          pageName.append(service.getSourcePath(node));
          pageName.append("\" style=\"font-size: 30px\" class=\"icon ");
          pageName.append(ICON);
          pageName.append(" v-table-icon-element\"></span>");
        } else {
          return node.getName();
        }
      } catch (RepositoryException e) {
        LOG.error("Could not generate custom name of node [{}]", node, e);
      }

      return pageName.toString();
    }

    return null;
  }
}
