package de.neoskop.magnolia.pagelinks;

import info.magnolia.jcr.util.NodeTypes;
import java.util.Arrays;
import java.util.List;

/**
 * This class is optional and represents the configuration for the page-links module. By exposing
 * simple getter/setter/adder methods, this bean can be configured via content2bean using the
 * properties and node from <tt>config:/modules/page-links</tt>. If you don't need this, simply
 * remove the reference to this class in the module descriptor xml.
 */
public class PageLinksModule {

  private String canonicalTagField = "";
  private List<String> synchronizedSystemProps =
      Arrays.asList(NodeTypes.LastModified.LAST_MODIFIED, NodeTypes.LastModified.LAST_MODIFIED_BY);

  public String getCanonicalTagField() {
    return canonicalTagField;
  }

  public void setCanonicalTagField(String canonicalTagField) {
    this.canonicalTagField = canonicalTagField;
  }

  public List<String> getSynchronizedSystemProps() {
    return synchronizedSystemProps;
  }

  public void setSynchronizedSystemProps(List<String> synchronizedSystemProps) {
    this.synchronizedSystemProps = synchronizedSystemProps;
  }
}
