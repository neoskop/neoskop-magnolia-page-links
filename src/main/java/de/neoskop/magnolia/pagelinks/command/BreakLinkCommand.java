package de.neoskop.magnolia.pagelinks.command;

import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.commands.impl.BaseRepositoryCommand;
import info.magnolia.context.Context;
import javax.inject.Inject;
import javax.jcr.Node;

/**
 * @author Arne Diekmann
 * @since 31.08.15
 */
public class BreakLinkCommand extends BaseRepositoryCommand {

  private final PageLinkService service;

  @Inject
  public BreakLinkCommand(PageLinkService service) {
    this.service = service;
  }

  @Override
  public boolean execute(Context context) throws Exception {
    final Node node = getJCRNode(context);
    service.breakLink(node);
    return true;
  }
}
