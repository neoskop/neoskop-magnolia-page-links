package de.neoskop.magnolia.pagelinks.command;

import static info.magnolia.repository.RepositoryConstants.WEBSITE;

import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.commands.impl.BaseRepositoryCommand;
import info.magnolia.context.Context;
import javax.inject.Inject;
import javax.jcr.Node;

/**
 * @author Arne Diekmann
 * @since 15.05.17
 */
public class SyncLinksCommand extends BaseRepositoryCommand {

  private final PageLinkService service;

  @Inject
  public SyncLinksCommand(PageLinkService service) {
    this.service = service;
  }

  @Override
  public boolean execute(Context context) throws Exception {
    final Node node = getJCRNode(context);
    service.synchronizeLinkedPages(node, context.getJCRSession(WEBSITE));
    return true;
  }
}
