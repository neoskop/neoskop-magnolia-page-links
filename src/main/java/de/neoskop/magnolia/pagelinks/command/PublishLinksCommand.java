package de.neoskop.magnolia.pagelinks.command;

import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.commands.CommandsManager;
import info.magnolia.commands.impl.RuleBasedCommand;
import info.magnolia.context.Context;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.module.activation.commands.ActivationCommand;
import info.magnolia.objectfactory.Components;
import info.magnolia.repository.RepositoryConstants;
import java.util.Arrays;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 01.03.16
 */
public class PublishLinksCommand extends RuleBasedCommand {

  private static final Logger LOG = Logger.getLogger(PublishLinksCommand.class);
  private boolean recursive;

  @Override
  public boolean execute(Context ctx) throws Exception {
    final Node node = getJCRNode(ctx);
    final Session session = ctx.getJCRSession(RepositoryConstants.WEBSITE);

    if (isRecursive()) {
      executeRecursively(node, session);
    } else {
      publishLinkedNodes(node, session);
    }

    return true;
  }

  private void executeRecursively(Node node, Session session) throws RepositoryException {
    final NodeIterator nodeIt = node.getNodes();

    while (nodeIt.hasNext()) {
      final Node childNode = nodeIt.nextNode();
      publishLinkedNodes(childNode, session);
    }
  }

  private void publishLinkedNodes(Node node, Session session) throws RepositoryException {
    final PageLinkService service = Components.getComponent(PageLinkService.class);
    final List<Node> links = service.findLinks(node.getIdentifier(), session);
    links
        .stream()
        .filter(service::isPageAClone)
        .filter(this::isPageOnPublicNodes)
        .forEach(this::sendActivationCommand);
  }

  private boolean isPageOnPublicNodes(Node node) {
    try {
      final int activationStatus = NodeTypes.Activatable.getActivationStatus(node);
      final List<Integer> onPublicNodeStates =
          Arrays.asList(
              NodeTypes.Activatable.ACTIVATION_STATUS_MODIFIED,
              NodeTypes.Activatable.ACTIVATION_STATUS_ACTIVATED);

      return onPublicNodeStates.stream().anyMatch(s -> activationStatus == s);
    } catch (RepositoryException e) {
      LOG.error("Could not determine node's activation status", e);
    }

    return false;
  }

  private void sendActivationCommand(Node node) {
    try {
      ActivationCommand activateCommand =
          (ActivationCommand) Components.getComponent(CommandsManager.class).getCommand("activate");
      activateCommand.setUuid(node.getIdentifier());
      activateCommand.setPath(node.getPath());
      activateCommand.setRepository(RepositoryConstants.WEBSITE);
      activateCommand.execute(MgnlContext.getInstance());
    } catch (Exception e) {
      LOG.error("Could not publish link", e);
    }
  }

  public boolean isRecursive() {
    return recursive;
  }

  public void setRecursive(boolean recursive) {
    this.recursive = recursive;
  }
}
