package de.neoskop.magnolia.pagelinks.rule;

import info.magnolia.ui.api.availability.AbstractAvailabilityRule;

/**
 * @author Arne Diekmann
 * @since 21.10.15
 */
public class NotLinkedOrDeviatedRule extends AbstractAvailabilityRule {

  @Override
  protected boolean isAvailableForItem(Object itemId) {
    return !new BelongsToALinkRule().isAvailableForItem(itemId)
        || new HasDeviatedRule().isAvailableForItem(itemId);
  }
}
