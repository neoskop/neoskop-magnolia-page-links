package de.neoskop.magnolia.pagelinks.rule;

import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.SessionUtil;
import info.magnolia.ui.api.availability.AbstractAvailabilityRule;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrPropertyItemId;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arne Diekmann
 * @since 04.09.15
 */
public class IsNotALinkRule extends AbstractAvailabilityRule {

  private static final Logger LOG = LoggerFactory.getLogger(IsNotALinkRule.class);

  @Override
  protected boolean isAvailableForItem(Object itemId) {
    if (itemId instanceof JcrItemId && !(itemId instanceof JcrPropertyItemId)) {
      JcrItemId jcrItemId = (JcrItemId) itemId;
      Node node = SessionUtil.getNodeByIdentifier(jcrItemId.getWorkspace(), jcrItemId.getUuid());
      if (node != null) {
        try {
          return !NodeUtil.hasMixin(node, PageLinkService.MIXIN_NAME);
        } catch (RepositoryException e) {
          LOG.warn(
              "Error evaluating availability for node [{}], returning false: {}",
              NodeUtil.getPathIfPossible(node),
              e.getMessage());
        }
      }
    }

    return false;
  }
}
