package de.neoskop.magnolia.pagelinks.rule;

import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.jcr.util.SessionUtil;
import info.magnolia.objectfactory.Components;
import info.magnolia.ui.api.availability.AbstractAvailabilityRule;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrPropertyItemId;
import javax.jcr.Node;

/**
 * @author Arne Diekmann
 * @since 15.05.17
 */
public class HasLinksRule extends AbstractAvailabilityRule {

  @Override
  protected boolean isAvailableForItem(Object itemId) {
    if (itemId instanceof JcrItemId && !(itemId instanceof JcrPropertyItemId)) {
      JcrItemId jcrItemId = (JcrItemId) itemId;
      Node node = SessionUtil.getNodeByIdentifier(jcrItemId.getWorkspace(), jcrItemId.getUuid());

      if (node != null) {
        return !Components.getComponent(PageLinkService.class).findLinks(node).isEmpty();
      }
    }

    return false;
  }
}
