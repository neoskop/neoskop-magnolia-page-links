package de.neoskop.magnolia.pagelinks.rule;

import de.neoskop.magnolia.pagelinks.service.PageLinkService;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.SessionUtil;
import info.magnolia.ui.api.availability.AbstractAvailabilityRule;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrPropertyItemId;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arne Diekmann
 * @since 16.09.15
 */
public class HasDeviatedRule extends AbstractAvailabilityRule {

  private static final Logger LOG = LoggerFactory.getLogger(HasDeviatedRule.class);

  @Override
  protected boolean isAvailableForItem(Object itemId) {
    if (itemId instanceof JcrItemId && !(itemId instanceof JcrPropertyItemId)) {
      JcrItemId jcrItemId = (JcrItemId) itemId;
      Node node = SessionUtil.getNodeByIdentifier(jcrItemId.getWorkspace(), jcrItemId.getUuid());

      if (node != null) {
        try {
          Node nodeToCheck = node;

          while (!nodeToCheck.isNodeType(NodeTypes.Area.NAME)
              && !nodeToCheck.isNodeType(NodeTypes.Page.NAME)) {
            nodeToCheck = nodeToCheck.getParent();
          }

          return NodeUtil.hasMixin(nodeToCheck, PageLinkService.DEVIATION_MIXIN_NAME);
        } catch (RepositoryException e) {
          LOG.warn(
              "Error evaluating availability for node [{}], returning true: {}",
              NodeUtil.getPathIfPossible(node),
              e.getMessage());
        }
      }
    }

    return true;
  }
}
